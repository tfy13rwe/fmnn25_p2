import numpy as np
from scipy.optimize import minimize
import scipy.linalg as sl
import matplotlib
import matplotlib.cm as cm
import matplotlib.pyplot as plt


class NewtonMethod:
    def __init__(self, problem):
        self.problem = problem
        pass

    def solve(self, exact = None):
        gra = self.problem.gradient
        n = self.problem.dimensions
        xk = np.array([-0.3, -0.8])  # initial guess
        steps = [np.copy(xk)]
        k = 0
        while not np.allclose(gra(xk), [0] * n):
            k += 1
            """
            Gk = self.hessian(gra, xk, n)
            cho = sl.cho_factor(Gk)
            alpha = 1
            x = sl.cho_solve(cho, alpha * gra(xk))
            xk = xk - x
            
            """
            Gk = self.hessian(gra, xk, n)
            if exact == None:
                try:
                    cho = sl.cho_factor(Gk)
                    alpha = 1
                    x = sl.cho_solve(cho, alpha * gra(xk))
                    xk += - x
                except sl.LinAlgError:
                   raise sl.LinAlgError('The Hessian of the objective function is not positive definite in', xk)
            if exact == True:
                sk = -np.linalg.inv(Gk) @ gra(xk)
                alpha=self.exactLineSearch(xk, sk)
                xk += alpha*sk
            elif exact == False:
                sk = -Gk @ gra(xk)
                alpha=self.inexactLineSearch(xk, sk)
                xk += alpha*sk
            steps.append(np.copy(xk))
            if k > 10000:
                print('did not converge')
                break
        return steps  
                

    def hessian(self, gradient,x,n,H=1e-6):
        G=np.zeros([n,n])
        for j in range(n):
            ej=np.zeros(np.shape(x))
            ej[j]=1
            h=H*x[j]
            G1 = (gradient(x + h * ej) - gradient(x)) / (h)
            h=x[j]*H
            G1 = (self.problem.gradient(x + h * ej) - self.problem.gradient(x)) / (h)
            G[:,j]=G1
        G=(G+np.transpose(G))/2
        return G
    

    def exactLineSearch(self, xk, sk):
         fbar = 0 #TODO how to choose lower bound on function?
         rho = 0.1
         funcval = self.problem.objFunc(xk)
         mu = (-fbar+funcval)/(rho*np.linalg.norm(self.problem.gradient(xk)))       
         alpha0 = mu/10
         fAlpha = lambda alpha0: self.problem.objFunc(xk + alpha0 * sk)
         alphaopt = minimize(fAlpha, alpha0).x[0]
         return alphaopt

    def inexactLineSearch(self, xk, sk):
        fAlpha = lambda a: self.problem.objFunc(xk + a *sk)
        fPrimAlpha = lambda a: np.dot(self.problem.gradient(xk + a*sk), sk)
        rho = 0.1
        #sigma = 0.7 # used in a different variation of LC and RC
        tau = 0.1
        chi = 9.0
        alpha0 = 1 #guess
        alphaL = 0
        alphaU = 10e99
        LC = False
        RC = False
        while not (LC and RC):
            #print(alpha0)
            #print(alphaL)
            #print(alphaU)
            if alphaU == alphaL:
                break
            if not LC:
                if np.isclose(alphaL,alpha0):
                    deltaAlpha0 = 0
                else:
                    deltaAlpha0 = (alpha0 - alphaL)*fPrimAlpha(alpha0)/(fPrimAlpha(alphaL)-fPrimAlpha(alpha0))
                deltaAlpha0 = max(deltaAlpha0, tau * (alpha0 - alphaL))
                deltaAlpha0 = min(deltaAlpha0, chi * (alpha0 - alphaL))
                alphaL = alpha0
                alpha0 = alpha0 + deltaAlpha0
            else:
                alphaU = min(alpha0, alphaU)
                if alphaU == alphaL:
                    break
                alphaBar0 = (((alpha0-alphaL)**2)*fPrimAlpha(alphaL)) / (2*(fAlpha(alphaL) - fAlpha(alpha0) + (alpha0 - alphaL)*fPrimAlpha(alphaL)))
                alphaBar0 = max(alphaBar0, alphaL + tau*(alphaU - alphaL))
                alphaBar0 = min(alphaBar0, alphaU - tau*(alphaU - alphaL))
                alpha0 = alphaBar0

            LC = fAlpha(alpha0) >= fAlpha(alphaL) + \
                 (1 - rho) * (alpha0 - alphaL) * fPrimAlpha(alphaL)
            RC = fPrimAlpha(alpha0) <= fAlpha(alphaL) + \
                 rho*(alpha0 - alphaL) * fPrimAlpha(alphaL)
        return alpha0

class Problem:

    def __init__(self, objFunc,dimensions, gradient=None,):
        self.objFunc = objFunc
        self.dimensions = dimensions
        self.gradient = gradient
        if not gradient:
            self.gradient = self.computeGradient()

    def computeGradient(self):
        grad = []
        I = np.identity(self.dimensions)
        h = lambda x: x * 10 ** -8 if x != 0 else 10 ** -8
        grad.append(lambda x: np.array([(self.objFunc(x + I[:, 0]*h(x[0])) - self.objFunc(x))/h(x[0])]))
        for i in range(self.dimensions - 1):
            k = i + 1
            grad.append(lambda x: np.append(grad[k-1](x),(self.objFunc(x + I[:, k]*h(x[k])) - self.objFunc(x))/h(x[k])))
        return grad[self.dimensions-1]


def rosenbrock(x):
    return 100*(x[1] - x[0]**2)**2 + (1 - x[0])**2



#rosenbrock = lambda x: 100*(x[1] - x[0]**2)**2 + (1 - x[0])**2

np.seterr(all='raise')

p = Problem(rosenbrock, 2)



#print(p.get_h()(0.1))

number = p.gradient(np.array([0.1,0.2]))
#print(number)

met = NewtonMethod(p)

print('scipy minimize', minimize(rosenbrock, np.array([5.0,5.0])).x)

#print(met.exactLineSearch(np.array([1,2]), np.array([0.5,0.5])))


steps = met.solve(False)
print('our newton', steps[-1])

#print(met.inexactLineSearch(np.array([1,2]), np.array([0.5,0.5])))
def rosen2(x, y):
    return 100*(y - x**2)**2 + (1 - x)**2

delta = 0.005
x = np.arange(-0.5, 2.0, delta)
y = np.arange(-1.5, 4.0, delta)
X, Y = np.meshgrid(x, y)
Z = rosen2(X,Y)
plt.contour(X, Y, Z, np.logspace(-0.5,3.5,20,base=10))
step_x = [i[0] for i in steps]
step_y = [i[1] for i in steps]
plt.plot(step_x, step_y, 'o')

#print(steps)